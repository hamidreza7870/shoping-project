import React, { useState } from 'react';
import Select from 'react-select';
import './SelectItem.css';

const options = [
  { value: 'laptop', label: 'لپ تاپ' },
  { value: 'phone', label: 'موبایل' },
  { value: 'camera', label: 'دوربین دیجیتال' },
  { value: 'shirt', label: 'پیراهن' },
  { value: 'shoes', label: 'کفش' },
  { value: 'bag', label: 'کیف' },
  { value: 'watch', label: 'ساعت' },
  { value: 'headphone', label: 'هدفون' },
  { value: 'field1', label: 'کلاه' },
  { value: 'field2', label: 'کوله پشتی' },
  { value: 'field3', label: 'کیف پول' },
  { value: 'field4', label: 'کامپیوتر' },
];

function SelectItem() {
  const [SelectedOption, setSelectedOption] = useState(1);

  const handleChange = e => {
    setSelectedOption({ selectedOption }, () => console.log(`Option selected:`, SelectedOption));
  };
  const { selectedOption } = SelectedOption;

  return (
    <Select
      placeholder="دسته بندی"
      value={selectedOption}
      onChange={handleChange}
      options={options}
    />
  );
}
export default SelectItem;

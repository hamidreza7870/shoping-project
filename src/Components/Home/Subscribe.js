import React from 'react';
import './Subscribe.css';
function Subscribe(params) {
  return (
    <>
      <div className="subscribe">
        <div className="subscribe-container">
          <div className="subscribe-text">
            <div className="subscribe-icon">
              <span className="icon-envelope"></span>
            </div>
            <div className="subscribe-text-left">
              <h3 className="subscribe-title">عضویت در خبرنامه</h3>
              <h5 className="subscribe-tagline">
                برای آگاهی از جدیدترین محصولات درخبرنامه عضو شوید
              </h5>
            </div>
          </div>
          <div className="subscribe-form">
            <form className="form-inline">
              <div className="form-group">
                <input type="text" className="form-control" placeholder="ایمیل خود را وارد کنید" />
                <button type="submit" className="btn-form">
                  عضویت
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default Subscribe;

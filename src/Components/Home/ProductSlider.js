import React from 'react';
import Rating from '../Rating';
import Samsunggear1 from '../../assets/media/samsung-gear1.jpg';
import Samsunggear2 from '../../assets/media/samsung-gear2.jpg';
import Backpack0 from '../../assets/media/backpack0.jpg';
import Backpack1 from '../../assets/media/backpack1.jpg';
import Watch2 from '../../assets/media/watch2.jpg';
import Watch3 from '../../assets/media/watch3.jpg';
import TV1 from '../../assets/media/TV1.jpg';
import TV2 from '../../assets/media/TV2.jpg';
import Smartwatch30 from '../../assets/media/smartwatch30.jpg';
import Smartwatch31 from '../../assets/media/smartwatch31.jpg';
import Hybridsmartwatch11 from '../../assets/media/hybridsmartwatch11.jpg';
import Hybridsmartwatch22 from '../../assets/media/hybridsmartwatch22.jpg';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import './ProductSlider.css';
import { Link } from 'react-router-dom';
function ProductSlider(params) {
  return (
    <div className="container-slider">
      <OwlCarousel className="owl-theme" items="5" nav dosts="none" loop>
        <div className="Product-slider">
          <div className="Product-slider-box">
            <div className="paoduct-icon">
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-searchQuick icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-like icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-compare icon-products"></span>
                </Link>
              </div>
            </div>
            <img src={Samsunggear1} alt="1" className="prouduct-list1" />
            <img src={Samsunggear2} alt="1" className="prouduct-list-hidden" />
            <Rating />
            <Link className="product-slider-tagline">
              <h3 className="Product-slider-title">Samsung Gear VR</h3>
              <h3 className="Product-slider-price">تومان 8,500,000</h3>
              <span className="product-search-icon"></span>
              <span className="product-wishlist-icon"></span>
              <span className="product-compare-icon"></span>
            </Link>
          </div>
        </div>

        <div className="Product-slider">
          <div className="Product-slider-box">
            <div className="paoduct-icon">
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-searchQuick icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-like icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-compare icon-products"></span>
                </Link>
              </div>
            </div>
            <img src={TV1} alt="1" className="prouduct-list1" />
            <img src={TV2} alt="1" className="prouduct-list-hidden" />
            <Rating />
            <Link className="product-slider-tagline">
              <h3 className="Product-slider-title">Roku Smart LED TV</h3>
              <h3 className="Product-slider-price">تومان 3,500,000</h3>
            </Link>
          </div>
        </div>

        <div className="Product-slider">
          <div className="Product-slider-box">
            <div className="paoduct-icon">
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-searchQuick icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-like icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-compare icon-products"></span>
                </Link>
              </div>
            </div>
            <img src={Hybridsmartwatch11} alt="1" className="prouduct-list1" />
            <img src={Hybridsmartwatch22} alt="1" className="prouduct-list-hidden" />
            <Rating />
            <Link className="product-slider-tagline">
              <h3 className="Product-slider-title">Neely Hybrid Smartwatch</h3>
              <h3 className="Product-slider-price">تومان 7,500,000</h3>
            </Link>
          </div>
        </div>

        <div className="Product-slider">
          <div className="Product-slider-box">
            <div className="paoduct-icon">
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-searchQuick icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-like icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-compare icon-products"></span>
                </Link>
              </div>
            </div>
            <span className="new">NEW</span>
            <img src={Watch2} alt="1" className="prouduct-list1" />
            <img src={Watch3} alt="1" className="prouduct-list-hidden" />
            <Rating />
            <Link className="product-slider-tagline">
              <h3 className="Product-slider-title">Apple Watch Series 38</h3>
              <h3 className="Product-slider-price">تومان 8,500,000</h3>
            </Link>
          </div>
        </div>

        <div className="Product-slider">
          <div className="Product-slider-box">
            <div className="paoduct-icon">
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-searchQuick icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-like icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-compare icon-products"></span>
                </Link>
              </div>
            </div>
            <span className="hot">HOT</span>
            <img src={Backpack0} alt="1" className="prouduct-list1" />
            <img src={Backpack1} alt="1" className="prouduct-list-hidden" />
            <Rating />
            <Link className="product-slider-tagline">
              <h3 className="Product-slider-title">Coolcy Backpack Purse</h3>
              <h3 className="Product-slider-price">تومان 1,500,000</h3>
            </Link>
          </div>
        </div>

        <div className="Product-slider">
          <div className="Product-slider-box">
            <div className="paoduct-icon">
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-searchQuick icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-like icon-products"></span>
                </Link>
              </div>
              <div className="product-view">
                <Link className="flag-link">
                  <span className="icon-compare icon-products"></span>
                </Link>
              </div>
            </div>
            <img src={Smartwatch30} alt="1" className="prouduct-list1" />
            <img src={Smartwatch31} alt="1" className="prouduct-list-hidden" />
            <Rating />
            <Link className="product-slider-tagline">
              <h3 className="Product-slider-title">Q Hybrid Smartwatch</h3>
              <h3 className="Product-slider-price">تومان 1,500,000</h3>
            </Link>
          </div>
        </div>
      </OwlCarousel>
    </div>
  );
}
export default ProductSlider;

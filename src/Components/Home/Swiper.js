import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import './Swiper.css';
import { Link } from 'react-router-dom';

function Swiper(params) {
  return (
    <>
      <div className="swiper">
        <OwlCarousel className="owl-theme" items="7" nav dosts="none" loop>
          <div className="box-swiper item">
            <Link className="link-swiper">
              <span className="icon-swiper icon-camera"></span>
              <span className="title-box-swiper">دوربین عکاسی</span>
            </Link>
          </div>

          <div className="box-swiper item">
            <Link className="link-swiper">
              <span className="icon-swiper icon-clothings"></span>
              <span className="title-box-swiper">پوشاک</span>
            </Link>
          </div>
          <div className="box-swiper item">
            <Link className="link-swiper">
              <span className="icon-swiper icon-game"></span>
              <span className="title-box-swiper">وسایل بازی</span>
            </Link>
          </div>
          <div className="box-swiper item">
            <Link className="link-swiper">
              <span className="icon-swiper icon-laptop"></span>
              <span className="title-box-swiper">لپ تاپ</span>
            </Link>
          </div>
          <div className="box-swiper item">
            <Link className="link-swiper">
              <span className="icon-swiper icon-phone"></span>
              <span className="title-box-swiper">تلفن همراه</span>
            </Link>
          </div>
          <div className="box-swiper item">
            <Link className="link-swiper">
              <span className="icon-swiper icon-shoes"></span>
              <span className="title-box-swiper">کفش</span>
            </Link>
          </div>
          <div className="box-swiper item">
            <Link className="link-swiper">
              <span className="icon-swiper icon-tv"></span>
              <span className="title-box-swiper">تلویزیون</span>
            </Link>
          </div>
          <div className="box-swiper item">
            <Link className="link-swiper">
              <span className="icon-swiper icon-watch"></span>
              <span className="title-box-swiper">ساعت</span>
            </Link>
          </div>
          <div className="box-swiper item">
            <Link className="link-swiper">
              <span className="icon-swiper icon-backpack"></span>
              <span className="title-box-swiper">کوله پشتی و کیف پول</span>
            </Link>
          </div>
        </OwlCarousel>
      </div>
    </>
  );
}
export default Swiper;

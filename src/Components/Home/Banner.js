import React from 'react';
import Banner1 from '../../assets/media/banner1.jpg';
import Banner2 from '../../assets/media/banner2.jpg';
import Banner3 from '../../assets/media/banner3.jpg';
import './Banner.css';
function Banner(params) {
  return (
    <>
      <div className="banner-container">
        <div className="banner-a">
          <div className="banner">
            <img alt="banner" src={Banner1} className="img-banner" />
          </div>
          <div className="banner">
            <img alt="banner" src={Banner2} className="img-banner" />
          </div>
          <div className="banner">
            <img alt="banner" src={Banner3} className="img-banner" />
          </div>
        </div>
      </div>
    </>
  );
}
export default Banner;

import React, { useState, useEffect } from 'react';
import gsap from 'gsap';
import './GsapSlider.css';
function GsapSlider(params) {
	const [active, setActive] = useState(0);
	const slides = [
		{
			id: 1,
			img: require('../../assets/media/one.jpg'),
			title_top: 'Trendy accessories',
			title_bottom: 'to Pop up your look',
			description: 'Trendy women collection',
		},
		{
			id: 2,
			img: require('../../assets/media/two.jpg'),
			title_top: 'Rebrand your look',
			title_bottom: 'Exclusive men wears',
			description: 'Get yourself looking wonderful',
		},
	];
	const { title_top, title_bottom, description, img } = slides[active];

	useEffect(() => {
		const tl = gsap.timeline({
			paused: true,
			onComplete: () => {
				setTimeout(chnageSlide, 3000);
			},
		});
		tl.from('.slideItem__desc', {
			duration: 1,
			opacity: 0,
			y: -50,
			ease: 'elastic.out(2.5, 0.75)',
		});
		tl.from(
			'.slideItem__name ',
			{
				duration: 1,
				opacity: 0,
				x: 300,
				stagger: 0.5,
			},
			'-=1',
		);
		tl.from('.slideItem__btn', {
			duration: 1,
			opacity: 0,
			ease: 'elastic.out(2.5, 0.75)',
			y: 50,
			stagger: 0.5,
		});
		gsap.to('.slideItem__sides', {
			opacity: 1,
			duration: 0.3,
			onComplete: () => {
				tl.play();
			},
		});
	}, [active]);

	const chnageSlide = slide => {
		gsap.to('.slideItem__sides', {
			opacity: 0,
			duration: 0.2,
			onComplete: () => {
				setActive(active => slide || (active + 1 === slides.length ? 0 : active + 1));
			},
		});
	};
	return (
		<div clas="gsap">
			<div className="container-gsap">
				<div className="slideItem slideItem__sides">
					<div className="slideItem__image ">
						<img className="slideItem__img" alt="pic" src={img} />
					</div>
					<div className="slideItem__text slideItem__sides">
						<p className="slideItem__desc">{description}</p>
						<h2 className="slideItem__name">{title_top}</h2>
						<h2 className="slideItem__name">{title_bottom}</h2>
						<button className="slideItem__btn">shop now</button>
					</div>
				</div>
			</div>
		</div>
	);
}
export default GsapSlider;

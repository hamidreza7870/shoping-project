import React from 'react';
import './Offers.css';
import './ProductSlider.css';
import Rating from '../Rating';
import { Link } from 'react-router-dom';
import banner21 from '../../assets/media/banner21.jpg';
import infraredHairdryer1 from '../../assets/media/infraredHairdryer1.jpg';
import infraredHairdryer2 from '../../assets/media/infraredHairdryer2.jpg';
import hoodie1 from '../../assets/media/hoodie1.jpg';
import hoodie2 from '../../assets/media/hoodie2.jpg';
import headphone1 from '../../assets/media/headphone1.jpg';
import headphone2 from '../../assets/media/headphone2.jpg';
import Samsunggear1 from '../../assets/media/samsung-gear1.jpg';
import Samsunggear2 from '../../assets/media/samsung-gear2.jpg';
import hybridsmartwatch1 from '../../assets/media/hybridsmartwatch1.jpg';
import hybridsmartwatch2 from '../../assets/media/hybridsmartwatch2.jpg';
import smartwatch30 from '../../assets/media/smartwatch30.jpg';
import smartwatch31 from '../../assets/media/smartwatch31.jpg';
import galaxyj81 from '../../assets/media/galaxyj81.jpg';
import galaxyj82 from '../../assets/media/galaxyj82.jpg';
import versasmartwatch1 from '../../assets/media/versasmartwatch1.jpg';
import versasmartwatch2 from '../../assets/media/versasmartwatch2.jpg';
import digitalcamera from '../../assets/media/digitalcamera.jpg';
import iPhoneXS from '../../assets/media/iPhoneXS.jpg';
import laptop from '../../assets/media/laptop.jpg';
import hybridsmartwatch3mini from '../../assets/media/hybridsmartwatch3mini.jpg';
import acerlogo from '../../assets/media/logo/acer-logo.png';
import burberrylogo from '../../assets/media/logo/burberry-logo.png';
import DandGlogo from '../../assets/media/logo/D-and-G-logo.png';
import fitbitlogo from '../../assets/media/logo/fitbit-logo.png';
import hoganlogo from '../../assets/media/logo/hogan-logo.png';
import newbalancelogo from '../../assets/media/logo/new-balance-logo.png';
import samsunglogo from '../../assets/media/logo/samsung-logo.png';
import acerlsonoslogoogo from '../../assets/media/logo/sonos-logo.png';
import sonylogo from '../../assets/media/logo/sony-logo.png';
import tcllogo from '../../assets/media/logo/tcl-logo.png';
import valentinologo from '../../assets/media/logo/valentino-logo.png';
import guccilogo from '../../assets/media/logo/gucci-logo.png';
function Offers(params) {
  return (
    <>
      <section className="suggestion">
        <div className="offers-container">
          <div className="row content-layout">
            <div className="offers-sidebar">
              <div className="grid-item item-a">
                <div className="banner-sidebar">
                  <img
                    alt="banner1"
                    src={banner21}
                    className="img-banner-sidebar img-banner"
                    style={{ width: '283px', height: '423px' }}
                  />
                </div>
              </div>
              <div className="title-sidebar">
                <h3 className="hot-sidebar-title">داغ ترین ها</h3>
              </div>
              <div className="grid-item item-b">
                <div className="list-product">
                  <Link className="product-teaser">
                    <div className="teaser-img">
                      <img src={digitalcamera} alt="teaser" className="teaser-photo" />
                    </div>
                    <div className="teaser-meta">
                      <div className="teaser-title">Sony DSCW800/B Digital Camera</div>
                      <Rating />
                      <div className="teaser-price">7,700,000 تومان</div>
                    </div>
                  </Link>
                </div>

                <div className="list-product">
                  <Link className="product-teaser">
                    <div className="teaser-img">
                      <img src={laptop} alt="teaser" className="teaser-photo" />
                    </div>
                    <div className="teaser-meta">
                      <div className="teaser-title">
                        Acer Aspire E15, 15.6" 8th Gen Intel Core i3
                      </div>
                      <Rating />
                      <div className="teaser-price">11,750,000 تومان</div>
                    </div>
                  </Link>
                </div>

                <div className="list-product">
                  <Link className="product-teaser">
                    <div className="teaser-img">
                      <img src={iPhoneXS} alt="teaser" className="teaser-photo" />
                    </div>
                    <div className="teaser-meta">
                      <div className="teaser-title">Apple iPhone XS Max 256 GB - Gold</div>
                      <Rating />
                      <div className="teaser-price">18,000,000 تومان</div>
                    </div>
                  </Link>
                </div>

                <div className="list-product">
                  <Link className="product-teaser">
                    <div className="teaser-img">
                      <img src={hybridsmartwatch3mini} alt="teaser" className="teaser-photo" />
                    </div>
                    <div className="teaser-meta">
                      <div className="teaser-title">Jacqueline Hybrid Smartwatch</div>
                      <Rating />
                      <div className="teaser-price">6,550,000 تومان</div>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
            <div className="main-content left-contents">
              <div className="grid-item item-c">
                <h2 className="offers-title">شاید خوشتان بیاید</h2>
              </div>
              <div className="grid-item item-d">
                <div className="Product-slider">
                  <div className="Product-slider-box">
                    <span className="hot">HOT</span>
                    <div className="paoduct-icon">
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-searchQuick icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-like icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-compare icon-products"></span>
                        </Link>
                      </div>
                    </div>
                    <img src={headphone1} alt="1" className="prouduct-list2" />
                    <img src={headphone2} alt="1" className="prouduct-list-hidden2" />
                    <Rating />
                    <Link className="product-slider-tagline">
                      <h3 className="Product-slider-title">Sony Wireless Headphones - Black</h3>
                      <h3 className="Product-slider-price">تومان 5,000,000</h3>
                    </Link>
                  </div>
                </div>

                <div className="Product-slider">
                  <div className="Product-slider-box">
                    <div className="paoduct-icon">
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-searchQuick icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-like icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-compare icon-products"></span>
                        </Link>
                      </div>
                    </div>
                    <img src={Samsunggear1} alt="1" className="prouduct-list2" />
                    <img src={Samsunggear2} alt="1" className="prouduct-list-hidden2" />
                    <Rating />
                    <Link className="product-slider-tagline">
                      <h3 className="Product-slider-title">Samsung Gear VR</h3>
                      <h3 className="Product-slider-price">تومان 6,800,000</h3>
                    </Link>
                  </div>
                </div>

                <div className="Product-slider">
                  <div className="Product-slider-box">
                    <div className="paoduct-icon">
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-searchQuick icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-like icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-compare icon-products"></span>
                        </Link>
                      </div>
                    </div>
                    <img src={hoodie1} alt="1" className="prouduct-list2" />
                    <img src={hoodie2} alt="1" className="prouduct-list-hidden2" />
                    <Rating />
                    <Link className="product-slider-tagline">
                      <h3 className="Product-slider-title">Pullover Hoodie Sweatshirt - M</h3>
                      <h3 className="Product-slider-price">تومان 1,700,000</h3>
                    </Link>
                  </div>
                </div>

                <div className="Product-slider">
                  <div className="Product-slider-box">
                    <div className="paoduct-icon">
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-searchQuick icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-like icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-compare icon-products"></span>
                        </Link>
                      </div>
                    </div>
                    <img src={infraredHairdryer1} alt="1" className="prouduct-list2" />
                    <img src={infraredHairdryer2} alt="1" className="prouduct-list-hidden2" />
                    <Rating />
                    <Link className="product-slider-tagline">
                      <h3 className="Product-slider-title">Revlon Infrared Hair Dryer</h3>
                      <h3 className="Product-slider-price">تومان 5,000,000</h3>
                    </Link>
                  </div>
                </div>

                <div className="Product-slider">
                  <div className="Product-slider-box">
                    <span className="hot">HOT</span>
                    <div className="paoduct-icon">
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-searchQuick icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-like icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-compare icon-products"></span>
                        </Link>
                      </div>
                    </div>
                    <img src={hybridsmartwatch1} alt="1" className="prouduct-list2" />
                    <img src={hybridsmartwatch2} alt="1" className="prouduct-list-hidden2" />
                    <Rating />
                    <Link className="product-slider-tagline">
                      <h3 className="Product-slider-title">Jacqueline Hybrid Smartwatch</h3>
                      <h3 className="Product-slider-price">تومان 5,000,000</h3>
                    </Link>
                  </div>
                </div>

                <div className="Product-slider">
                  <div className="Product-slider-box">
                    <div className="paoduct-icon">
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-searchQuick icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-like icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-compare icon-products"></span>
                        </Link>
                      </div>
                    </div>
                    <img src={smartwatch30} alt="1" className="prouduct-list2" />
                    <img src={smartwatch31} alt="1" className="prouduct-list-hidden2" />
                    <Rating />
                    <Link className="product-slider-tagline">
                      <h3 className="Product-slider-title">Q Hybrid Smartwatch</h3>
                      <h3 className="Product-slider-price">تومان 6,800,000</h3>
                    </Link>
                  </div>
                </div>

                <div className="Product-slider">
                  <div className="Product-slider-box">
                    <div className="paoduct-icon">
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-searchQuick icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-like icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-compare icon-products"></span>
                        </Link>
                      </div>
                    </div>
                    <img src={galaxyj81} alt="1" className="prouduct-list2" />
                    <img src={galaxyj82} alt="1" className="prouduct-list-hidden2" />
                    <Rating />
                    <Link className="product-slider-tagline">
                      <h3 className="Product-slider-title">Samsung Galaxy J8</h3>
                      <h3 className="Product-slider-price">تومان 1,700,000</h3>
                    </Link>
                  </div>
                </div>

                <div className="Product-slider">
                  <div className="Product-slider-box">
                    <div className="paoduct-icon">
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-searchQuick icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-like icon-products"></span>
                        </Link>
                      </div>
                      <div className="product-view">
                        <Link className="flag-link">
                          <span className="icon-compare icon-products"></span>
                        </Link>
                      </div>
                    </div>
                    <img src={versasmartwatch1} alt="1" className="prouduct-list2" />
                    <img src={versasmartwatch2} alt="1" className="prouduct-list-hidden2" />
                    <Rating style={{ marginTop: '2em' }} />
                    <Link className="product-slider-tagline">
                      <h3 className="Product-slider-title">Fitbit Versa Smartwatch</h3>
                      <h3 className="Product-slider-price">تومان 5,000,000</h3>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="grid-item item-f">
                <div className="logo-container">
                  <div className="logo">
                    <img src={acerlogo} alt="logo" className="logo-brand" />
                  </div>
                  <div className="logo">
                    <img src={burberrylogo} alt="logo" className="logo-brand" />
                  </div>
                  <div className="logo">
                    <img src={DandGlogo} alt="logo" className="logo-brand" />
                  </div>
                  <div className="logo">
                    <img src={fitbitlogo} alt="logo" className="logo-brand" />
                  </div>
                  <div className="logo">
                    <img src={hoganlogo} alt="logo" className="logo-brand" />
                  </div>
                  <div className="logo">
                    <img src={newbalancelogo} alt="logo" className="logo-brand" />
                  </div>
                  <div className="logo">
                    <img src={samsunglogo} alt="logo" className="logo-brand" />
                  </div>
                  <div className="logo">
                    <img src={acerlsonoslogoogo} alt="logo" className="logo-brand" />
                  </div>
                  <div className="logo">
                    <img src={sonylogo} alt="logo" className="logo-brand" />
                  </div>
                  <div className="logo">
                    <img src={tcllogo} alt="logo" className="logo-brand" />
                  </div>
                  <div className="logo">
                    <img src={valentinologo} alt="logo" className="logo-brand" />
                  </div>
                  <div className="logo">
                    <img src={guccilogo} alt="logo" className="logo-brand" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
export default Offers;

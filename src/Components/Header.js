import React from 'react';
import { Link } from 'react-router-dom';
import './Header.css';
import LOgo from '../assets/media/logo.png';
import SelectItem from './SelectItem';
function Header(props) {
  return (
    <div className="header-container">
      <div className="header-top">
        <div className="wrapper-top">
          <div className="header-top-right">
            <span>
              فروش محصولات تا 40% تخفیف . میتوانید همین حالا{' '}
              <Link className="link-off" to="/products">
                {' '}
                خرید کنید
              </Link>
            </span>
          </div>
          <div className="header-top-left">
            <Link className="account">
              ورود به حساب کاربری
              {/* <span className='downIcon icon-down1' ></span> */}
            </Link>
          </div>
        </div>
      </div>

      <div className="header-Center">
        <div className="header-wrapper">
          <div className="header-logo">
            <img alt="logo" src={LOgo} />
          </div>
          <div className="search">
            <form className="searchForm">
              <div className="categories">
                <SelectItem />
              </div>

              <div className="input-search">
                <input type="text" className="input-form" placeholder="جستجوی محصولات" />
              </div>

              <div className="button-search">
                <button type="submit" className="button-form">
                  <span className="icon-search" color={'white'}></span>
                </button>
              </div>
            </form>
          </div>
          <div className="call-number">
            <div className="call-box">
              <p className="tagline-call">تلفن تماس</p>
              <p className="number-call">021-12345678</p>
            </div>
            <Link to="/cart" className="order link">
              <span className="icon-cart"></span>
              <span className="counter-order">0</span>
            </Link>
          </div>
        </div>
      </div>

      <div className="header-bottom sticky">
        <div className="container-mega-menu">
          <h4 className="mega-menu">
            <span className="icon-menu">
              <span className="icon-menu1" style={{ width: '1.2em' }} />
            </span>
            <span className="mega-menu-title">طبقه بندی محصولات</span>
            <div className="drop-down-categories">
              <Link to="#" className="link drop-down-link">
                لوازم الکترونیکی
              </Link>
              <Link to="#" className="link drop-down-link">
                دوربین عکاسی
              </Link>
              <Link to="#" className="link drop-down-link">
                موبایل و تبلت
              </Link>
              <Link to="#" className="link drop-down-link">
                مد و لباس
              </Link>
              <Link to="#" className="link drop-down-link">
                لوازم بازی
              </Link>
              <Link to="#" className="link drop-down-link">
                لپ تاپ
              </Link>
            </div>
          </h4>
          <div className="main-menu">
            <Link className="link" to="/">
              {' '}
              خانه
            </Link>
            <Link className="link" to="/products">
              {' '}
              محصولات
            </Link>
            <Link className="link" to="/services">
              {' '}
              خدمات
            </Link>
            <Link className="link" to="/about">
              {' '}
              درباره ما
            </Link>
            <Link className="link" to="/contact">
              {' '}
              تماس با ما
            </Link>
          </div>
          <div className="delivery">
            <span className="icon-delivery"></span>
            <p className="delivery-message">ارسال رایگان برای خرید های بالای 500 هزار ریال</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;

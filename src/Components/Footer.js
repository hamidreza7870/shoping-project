import React from 'react'
import { Link } from 'react-router-dom'
import logo from '../assets/media/logo.png'
import paymentmethod from '../assets/media/paymentmethod.png'
import './Footer.css'
function Footer(params) {
    return (
        <div className='footer'>
          <div className='footer-widget'>
						<div className='footer-container'>
							<div className='footer-row'>
								<div className='region region-footer-first'>
									<div className='block-footer-about block'>
										<div className='footer-logo'>
											<img className='footer-image' alt='logo' src={logo} />
										</div>
										<div className='footer-textaboute'>
											<div className='footer-description'>
												<p className='descText'>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها </p>
												<div className='calldetails'>
													<span className='icon-phone footer-phone' style={{ fontSize: '1.5em' }}></span>
													<p style={{margin: '10px 0.5em',fontSize: '16px'}}>موبایل : +989121234567</p>
												</div>
												
												<div className='calldetails'>
													<span className='icon-envelope footer-email' style={{fontSize: '1.5em'}}></span>
													<p style={{margin: '10px 0.5em',fontSize: '16px'}}>ایمیل : info@website.com</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className='region region-footer-second'>
									<div className='block-quick-links block'>
										<div className='container-wrap'>
											<div className='block-title-wrap'>
												<div className='block-title-content'>
													<h2 className='block-title'>دسترسی سریع</h2>
												</div>
											</div>
											<div className='block-content'>
												<div className='field-body'>
													<ul className='menu-footer'>
														<li className='list-footer'><Link className='link-footer' to='/' >حساب کاربری</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' >حمل و نقل</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' >پرسش های متداول</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' >مقایسه محصولات</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' >حریم خصوصی</Link></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className='region region-footer-third'>
									<div className='block-about-links block'>
										<div className='container-wrap'>
											<div className='block-title-wrap'>
												<div className='block-title-content'>
													<h2 className='block-title'>درباره ما</h2>
												</div>
											</div>
											<div className='block-content'>
												<div className='field-body'>
													<ul className='menu-footer'>
														<li className='list-footer'><Link className='link-footer' to='/' >تاریخچه</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' >تخصص های ما</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' >شرایط قرارداد</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' > حفظ حریم شخصی</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' >تماس با ما</Link></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className='region region-footer-forth'>
									<div className='block-support-links block'>
										<div className='container-wrap'>
											<div className='block-title-wrap'>
												<div className='block-title'>
													<h2 className='block-title'>پشتیبانی</h2>
												</div>
											</div>
											<div className='block-content'>
												<div className='field-body'>
													<ul className='menu-footer'>
														<li className='list-footer'><Link className='link-footer' to='/' >حساب کاربری</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' >حمل و نقل</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' >پرسش های متداول</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' >مقایسه محصولات</Link></li>
														<li className='list-footer'><Link className='link-footer' to='/' >حریم خصوصی</Link></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className='region region-footer-fifth'>
										<div className='block-account-links block'>
											<div className='container-wrap'>
												<div className='block-title-wrap'>
													<div className='block-title-content'>
														<h2 className='block-title'>حساب کاربری</h2>
													</div>
												</div>
												<div className='block-content'>
													<div className='field-body'>
														<ul className='menu-footer'>
															<li className='list-footer'><Link className='link-footer' to='/' >اطلاعات شخصی</Link></li>
															<li className='list-footer'><Link className='link-footer' to='/' >سفارشات</Link></li>
															<li className='list-footer'><Link className='link-footer' to='/' >سبد خرید</Link></li>
															<li className='list-footer'><Link className='link-footer' to='/' >اطلاعات حساب</Link></li>
															<li className='list-footer'><Link className='link-footer' to='/' >نشانی ها</Link></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
					
          <div className='footer-bottom'>
						<div className='footer-container'>
							<div className='footer-row'>
								<div className='footer-bottom-first'>
									<div className='copyright'>
										<p className='copyright-p'>تمام حقوق برای این سایت محفوظ است و استفاده از مطالب سایت تنها با ذکر نام و درج لینک مستقیم مجاز است.</p>
									</div>
								</div>
								<div className='footer-bottom-second'>
									<div className='image-copyright'>
										<img alt='pic' src={paymentmethod} className='image-copyright-left' />
									</div>
								</div>
							</div>
						</div>
					</div>
        </div>
    )
}
export default Footer;
import React from 'react';
import Header from '../Components/Header';
import GsapSlider from '../Components/Home/GsapSlider';
import Swiper from '../Components/Home/Swiper';
import Banner from '../Components/Home/Banner';
import Timer from '../Components/Home/Timer';
import ProductSlider from '../Components/Home/ProductSlider';
import Offers from '../Components/Home/Offers';
import Subscribe from '../Components/Home/Subscribe';
import Footer from '../Components/Footer';

function Home(props) {
  return (
    <>
      <Header />
      <GsapSlider />
      <Swiper />
      <Banner />
      <Timer />
      <ProductSlider />
      <Offers />
      <Subscribe />
      <Footer />
    </>
  );
}
export default Home;

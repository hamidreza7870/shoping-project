import React from 'react';
import { Route } from 'react-router-dom';
import './App.css';
import Home from './Pages/Home';
import './assets/Font/iranyekan/WebFonts/css/style.css';
import './assets/icomoon/style.css'
// import './Font/iranyekan/WebFonts/css/fontiran.css';
function App() {
  return (
    <div className="App">
      {/* <Header /> */}
      <Route path='/' component={Home}/>
      <Route path='/contact' />
      <Route path='/products' />
      <Route path='/about' />
      <Route path='/services' />
      <Route path='/cart' />
    </div>
  );
}

export default App;
